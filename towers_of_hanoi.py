count = 0
def towers_of_hanoi(disks, source, dest, pivot):
    global count
    if disks >= 1:
        towers_of_hanoi(disks - 1, source, pivot, dest)
        print ("Move top disk from tower  { %s }    to top of tower  { %s }   " %(  source, dest))
        count = count + 1
        towers_of_hanoi(disks - 1, pivot, dest, source)
n = int(input("Enter the no of disks :"))
towers_of_hanoi(n, 'Source', 'Dest', 'Pivot')
print("count is %d" %count)
